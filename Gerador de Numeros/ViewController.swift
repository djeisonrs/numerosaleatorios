//
//  ViewController.swift
//  Gerador de Numeros
//
//  Created by Djeison Aquino on 16/08/17.
//  Copyright © 2017 Djeison Aquino. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    @IBAction func gerarNumero(_ sender: Any) {
        
        let numeroAleatorio = arc4random_uniform(11)
        
        numeroGerado.text = String(numeroAleatorio)
    }
    
    @IBOutlet weak var numeroGerado: UILabel!
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

